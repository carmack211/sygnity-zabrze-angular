import { AuthConfig } from 'src/app/security/auth.service';

export const environment = {
  production: true,
  api_url: 'https://api.spotify.com/v1/search',
  authConfig: {
    auth_url: "https://accounts.spotify.com/authorize",
    client_id: "11de6dcde791431baa6cb0d9f9bf35ba",
    redirect_uri: "http://localhost:4200/",
    response_type: "token"
  } as AuthConfig 
};
