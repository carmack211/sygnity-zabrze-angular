import { Injectable } from "@angular/core";
import { Playlist } from "../models/Playlist";
import { BehaviorSubject, of } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService {
  private playlists = new BehaviorSubject<Playlist[]>([
    {
      id: 123,
      name: "Angular Top20!",
      favorite: false,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "Best of Angular",
      favorite: true,
      color: "#ffff00"
    },
    {
      id: 345,
      name: "Angular Greatest Hits!",
      favorite: false,
      color: "#00ffff"
    }
  ]);

  playlistsChange = this.playlists.asObservable();

  getPlaylist(id) {
    const playlists = this.playlists.getValue();

    return of(playlists.find(p => p.id == id));
  }

  save(draft: Playlist) {
    const playlists = this.playlists
      .getValue()
      .map(p => (p.id == draft.id ? draft : p));

    this.playlists.next(playlists);
  }

  constructor() {}
}

// const index = playlists.findIndex(p=>p.id == draft.id)
// if(index !== -1){
//   playlists.splice(index,1,draft)
// }
