import { Component, OnInit, ViewEncapsulation, Input, EventEmitter, Output, ChangeDetectionStrategy } from "@angular/core";
import { Playlist } from "src/app/models/Playlist";
import { NgForOf, NgForOfContext } from "@angular/common";

NgForOf;
NgForOfContext;

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush
  // inputs: ["playlists:items"]
})
export class ItemsListComponent {

  @Input('items')
  playlists: Playlist[] = [];

  @Input()
  selected: Playlist

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(select: Playlist) {
    this.selectedChange.emit(select)

    // this.selected =
    //   this.selected && select.id === this.selected.id ? null : select;
  }

  trackFn(index,item){
    return item.id
  }

  constructor() {}

  ngOnInit() {}
}
